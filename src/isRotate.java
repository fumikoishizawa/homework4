

public class isRotate {

	public static String rotate(String s, int n){
		StringBuilder ans= new StringBuilder();
		int selectN=(s.length()-n)%s.length();
		
		for(int i=0; i<s.length(); i++){
			ans.append(s.charAt(selectN));
			selectN=(++selectN)%s.length();
		}
		
		return new String(ans);
	}
	
}
