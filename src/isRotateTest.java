import static org.junit.Assert.*;

import org.junit.Test;


public class isRotateTest {
	
	@Test
	public void test() { //normal test
		isRotate test1 = new isRotate();
		String answer = test1.rotate("abcde", 1);
		assertEquals(answer, "eabcd");
		answer=test1.rotate("abcde",-2);
		assertEquals(answer, "cdeab");
		answer=test1.rotate("abc", 3);
		assertEquals(answer,"abc");
		answer=test1.rotate("abc", -3);
		assertEquals(answer,"abc");
		answer=test1.rotate("abc", 0);
		assertEquals(answer,"abc");
	}
	
	@Test
	public void test2(){ // unique test
		isRotate test1 = new isRotate();
		String answer = test1.rotate(" ", 10);
		assertEquals(answer, " ");
		answer=test1.rotate(" ", 0);
		assertEquals(answer, " ");
	}
	//In this test, there is error when rotate("", 10) (line 2)
	//this error is "static...etc", so, my algorithm has error

	@Test
	public void test3(){ // capacity test
		isRotate test1 = new isRotate();
		String answer = test1.rotate("abc", 123456789);
		assertEquals(answer, "abc");
		answer=test1.rotate("abc", 2147483646);
		//the result agree with one of surplus
		String answer2 = test1.rotate("abc", 2147483646);
		assertEquals(answer,answer2);
	}
	//In this test, there is error when 2147483647 (line 4&6)
	//2147483647 is max value of integer, and 2147483646 has no error 
	//I think this means that my method don't have capacity of MAX_VALUE
	//We should test of MIN_VALUE
}


